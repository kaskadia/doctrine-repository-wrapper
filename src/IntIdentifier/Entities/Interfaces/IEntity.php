<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapper\IntIdentifier\Entities\Interfaces;

interface IEntity {
	public function getId(): int;
	public function setId(int $id): void;
}