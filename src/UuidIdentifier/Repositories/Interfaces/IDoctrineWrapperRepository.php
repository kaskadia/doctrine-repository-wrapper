<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapper\UuidIdentifier\Repositories\Interfaces;

use Doctrine\ORM\QueryBuilder;
use Kaskadia\Lib\DoctrineRepositoryWrapper\UuidIdentifier\Entities\Interfaces\IEntity;

interface IDoctrineWrapperRepository {

	/**
	 * Initialize the Doctrine::EntityRepository
	 *
	 * @param string $entity
	 */
	public function initializeRepository(string $entity): void;

	/**
	 * Finds an entity by its primary key / identifier.
	 * @param string $id
	 */
	public function find(string $id);

	/**
	 * Finds all the entities in the repository.
	 *
	 * @return array 
	 */
	public function findAll(): array;

	/**
	 * Finds the entities by a set of criteria.
	 *
	 * @param array $criteria
	 * @param array|null $orderBy
	 * @param int|null $limit
	 * @param int|null $offset
	 * @return array
	 */
	public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array;

	/**
	 * Finds a single entity by a set of criteria.
	 *
	 * @param array $criteria
	 */
	public function findOneBy(array $criteria);

	/**
	 * Saves the entity in the DB.
	 *
	 * @param IEntity $entity
	 * @return void
	 */
	public function save(IEntity $entity): void;

	/**
	 * Flush the unit of work
	 *
	 * @return void
	 */
	public function flush(): void;

	/**
	 * Save the entity and flush the unit of work
	 *
	 * @param IEntity $entity
	 * @return void
	 */
	public function saveAndFlush(IEntity $entity): void;

    /**
     * Returns the Doctrine Query Builder
     *
     * @param string $alias
     * @param string|null $indexBy
     * @return QueryBuilder
     */
    public function getQueryBuilder(string $alias, string $indexBy = null): QueryBuilder;
}
