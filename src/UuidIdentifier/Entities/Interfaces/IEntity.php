<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapper\UuidIdentifier\Entities\Interfaces;

interface IEntity {
	public function getId(): string;
	public function setId(string $id): void;
}