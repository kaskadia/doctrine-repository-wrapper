<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapper\UuidIdentifier\Entities;

use Kaskadia\Lib\DoctrineRepositoryWrapper\UuidIdentifier\Entities\Interfaces\IEntity;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\ORM\Mapping as ORM;

abstract class EntityBase implements IEntity {
	/**
	 * EntityBase constructor.
	 * @throws Exception
	 */
	public function __construct() {
		if(!isset($this->id)) {
			$this->id = Uuid::uuid4()->toString();
		}
	}

	/**
	 * @var string
	 * @ORM\Id
	 * @ORM\Column(type="uuid", unique=true)
	 * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
	 */
	protected string $id;

	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * Only sets id if unset. This is primarily for deserialization.
	 * We still want this property to be immutable.
	 * @param string $id
	 */
	public function setId(string $id): void {
		if(!isset($this->id)) {
			$this->id = $id;
		}
	}
}
