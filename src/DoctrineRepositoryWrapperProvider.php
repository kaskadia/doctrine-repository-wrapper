<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapper;

use Illuminate\Support\ServiceProvider;
use Kaskadia\Lib\DoctrineRepositoryWrapper\IntIdentifier\Repositories\DoctrineWrapperRepository as IntDoctrineWrapperRepository;
use Kaskadia\Lib\DoctrineRepositoryWrapper\UuidIdentifier\Repositories\DoctrineWrapperRepository as UuidDoctrineWrapperRepository;
use Kaskadia\Lib\DoctrineRepositoryWrapper\IntIdentifier\Repositories\Interfaces\IDoctrineWrapperRepository as IIntDoctrineWrapperRepository;
use Kaskadia\Lib\DoctrineRepositoryWrapper\UuidIdentifier\Repositories\Interfaces\IDoctrineWrapperRepository as IUuidDoctrineWrapperRepository;

class DoctrineRepositoryWrapperProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->bind(IIntDoctrineWrapperRepository::class, IntDoctrineWrapperRepository::class);
		$this->app->bind(IUuidDoctrineWrapperRepository::class, UuidDoctrineWrapperRepository::class);
	}
}