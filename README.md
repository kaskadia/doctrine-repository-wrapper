# Doctrine Repository Wrapper (deprecated)
This project has been split into two forks:
- [Doctrine Repository Wrapper UUID](https://gitlab.com/kaskadia/doctrine-repository-wrapper-uuid) 
  - This uses UUID based entities
- [Doctrine Repository Wrapper INT](https://gitlab.com/kaskadia/doctrine-repository-wrapper-int)
  - This uses INT based entities